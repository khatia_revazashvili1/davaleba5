package com.example.abcd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextRepeatPassword: EditText
    private lateinit var buttonRegister: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        init()
        profileListeners()

    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextRepeatPassword = findViewById(R.id.editTextRepeatPassword)
        buttonRegister = findViewById(R.id.buttonRegister)

    }

    private fun profileListeners() {
        buttonRegister.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val repeatPassword = editTextRepeatPassword.text.toString()



            if (email.isNotEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                    .matches()
            ) {
                Toast.makeText(this, "დადასტურებულია.", Toast.LENGTH_SHORT).show()


            } else {
                Toast.makeText(this, "E-mail არასწორია", Toast.LENGTH_SHORT).show()
            }



            if (password.length > 9 && password == repeatPassword && (password.contains('1')
                        || password.contains('2') || password.contains('3') || password.contains('4')
                        || password.contains('5') || password.contains('6') || password.contains('7')
                        || password.contains('8') || password.contains('9'))
            )

                Toast.makeText(this, "პაროლი დადასტურებულია", Toast.LENGTH_SHORT).show()
            else {
                Toast.makeText(this, "პაროლი უნდა შეიცავდეს მინიმუმ ერთ ციფრს", Toast.LENGTH_SHORT)
                    .show()
            }

            if (repeatPassword != password)
                Toast.makeText(this, "პაროლები არ ემთხვევა ერთმანეთს", Toast.LENGTH_SHORT).show()


            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener() { task ->
                    if (task.isSuccessful) {
                        gotoProfile()


                    } else{
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                }



        }
    }
    private fun gotoProfile(){
        startActivity(Intent(this,ProfileActivity::class.java))
        finish()
    }
}